# elastic docker compose

<div>
 <a href="https://www.elastic.co/">
   <img src="https://img.shields.io/badge/elasticsearch-8.14.1-blue">
 </a>
 <a href="https://www.elastic.co/kibana">
   <img src="https://img.shields.io/badge/kibana-8.14.1-blue">
 </a>
 <a href="https://www.elastic.co/logstash">
  <img src="https://img.shields.io/badge/logstash-8.14.1-blue">
 </a>
 <a href="https://www.elastic.co/beats/filebeat">
  <img src="https://img.shields.io/badge/filebeat-8.14.1-blue">
 </a>
 <a href="https://www.docker.com/">
  <img src="https://img.shields.io/badge/docker-latest-blue">
 </a>
 <a href="https://docs.docker.com/compose/">
  <img src="https://img.shields.io/badge/docker%20compose-latest-blue">
 </a>
</div>

## 簡介

elastic docker compose 是一個可以快速建立日誌收集器的伺服器，可以幫助使用者檢視在我們系統上，是否會有發生嚴重錯誤、CPU 標高、記憶體標高等，並且可以透過視覺化資料觀察我們系統日常狀況

## 開發

```bash
# 克隆項目
git clone https://gitlab.com/timmyBai/elastic_docker_compose.git
```

## elastic 之 volume 目錄擁有者調整

```bash
sudo chown USER:USER ~/Systems/elasticsearch
```

## 發佈

```bash
docker compose up --build -d
```

## GitLab Commit 格式

| 標籤       | 更改類型                        | 範例                                     |
| -------- | --------------------------- | -------------------------------------- |
| feat     | (feature)                   | feat(page): add page                   |
| fix      | (bug fix)                   | fix(page): add page bug                |
| docs     | (documentation)             | docs(documentation): add documentation |
| style    | (formatting, css, sass)     | style(sass): add page style            |
| refactor | (refactor)                  | refactor(page): refactor page          |
| test     | (when adding missing tests) | test(function): add function test      |
| chore    | (maintain)                  | chore(style): modify sass chore        |
